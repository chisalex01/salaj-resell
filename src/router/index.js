import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import PrivacyPolicyView from "../views/PrivacyPolicyView.vue";
import TermsAndConditionsView from "../views/TermsAndConditionsView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/privacy_policy",
      name: "privacy_policy",
      component: PrivacyPolicyView,
    },
    {
      path: "/terms_and_conditions",
      name: "terms_and_conditions",
      component: TermsAndConditionsView,
    },
    // {
    //   path: "/#produse",
    //   name: "produse",
    //   component: HomeView,
    // },
    // {
    //   path: "/#părere_clienți",
    //   name: "părere_clienți",
    //   component: HomeView,
    // },
    // {
    //   path: "/#contact",
    //   name: "contact",
    //   component: HomeView,
    // },
  ],
});

export default router;
