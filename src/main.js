import { createApp } from "vue";
import App from "./App.vue";
import Vue from "./App.vue";

import router from "./router";

import "aos/dist/aos.css";
import AOS from "aos";

import VideoBackground from "vue-responsive-video-background-player";
import { Plugin } from "vue-responsive-video-background-player";

// Vue.component("video-background", VideoBackground);

// Vue.use(Plugin);

const app = createApp(App);

app.use(router, AOS);

app.AOS = new AOS.init();

app.mount("#app");
